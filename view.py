# -*- coding: utf-8 -*-

import os
import re
import cgi
import json
import sqlite3

try:
    from BaseHTTPServer import HTTPServer
    from SimpleHTTPServer import SimpleHTTPRequestHandler
except:
    from http.server import HTTPServer
    from http.server import SimpleHTTPRequestHandler

    

class MyHandler(SimpleHTTPRequestHandler):
    
    image_dir = ''
    images = []
    
    def __init__(self, request, client_address, server):
        SimpleHTTPRequestHandler.__init__(self, request, client_address, server)
        
        con, cur = self._open()
        cur.execute("create table if not exists train (path text not null primary key, json text)")
        self._close(con, cur)
        
    
    def do_POST(self):
        
        route = self.path.split('/')[-1]
        if not hasattr(self, route):
            self.send_response(500)
            self.end_headers()
            return
        
        
        form = self._parse_form()
        response = getattr(self, route)(form)
        
        self.send_response(200)
        self.send_header("Content-type", 'application/json')
        self.send_header("Content-Length", len(response))
        self.end_headers()
        self.wfile.write(bytearray(response, encoding='utf8'))
        
        
    def _parse_form(self):
        if 'Content-Type' not in self.headers:
            return None
    
        return cgi.FieldStorage(
            fp=self.rfile, 
            headers=self.headers,
            environ={
                'REQUEST_METHOD':'POST',
                'CONTENT_TYPE':self.headers['Content-Type'],
            }
        )
    
        
        
    # --- router
    def _init(self, form):
        
        #最初の画像
        data = self._create_base_info(0);
        return json.dumps(data)
        
    def _back(self, form):
        pos = int(form['pos'].value)
        if pos > 0:
            pos -= 1
        
        data = self._create_base_info(pos);
        
        return json.dumps(data)
        
        
    def _next(self, form):

        skip = int(form['skip'].value)
        pos = int(form['pos'].value)
        next_pos = pos + 1
        
        #処理中の画像のパス
        image_path = os.path.join( MyHandler.image_dir, MyHandler.images[pos] )
        rects = []
        
        if skip == 0:
            #囲まれた範囲の座標
            rects = json.loads(form['rects'].value)
        
        self._log(image_path, skip, rects)
        
        
        #まだ画像があるか
        # 画像閲覧終了
        if next_pos >= len(MyHandler.images):
            self._finish_log()
            data = { 'pos' : next_pos, 'finished' : True }
            
        else:
            data = self._create_base_info(next_pos);
        
        return json.dumps(data)
        
    
    def _create_base_info(self, pos):
        
        imgsrc = os.path.join( MyHandler.image_dir, MyHandler.images[pos] )
        imgnum = len(MyHandler.images)
        counter = ''.join( [ str(pos + 1).zfill( len(str(imgnum)) ), ' of ', str(imgnum) ] )
        
        rects = self._get_rect_info(imgsrc)
        
        data = {
            'imgsrc' : imgsrc,
            'counter' : counter,
            'pos' : pos,
            'barwidth' : '%d%%' % (pos * 100 / imgnum),
            'finished' : False,
            'rects' : rects
        }
        
        return data
        
        
    def _get_rect_info(self, path):
        con, cur = self._open()
        
        cur.execute("select json from train where path = ?", (path,))
        row = cur.fetchone()
        
        self._close(con, cur)
        
        if not row:
            return []
        
        json_data = json.loads(row[0])
        return json_data['rects']
        
        
    def _log(self, path, skip, rects):
        
        con, cur = self._open()
        
        json_data = json.dumps({'skip':skip, 'rects':rects})
        cur.execute("insert or replace into train (path, json) values (?, ?)", (path, json_data))
        
        self._close(con, cur)
        
    def _finish_log(self):
    
        con, cur = self._open()
        
        cur.execute("select path, json from train")
        
        with open('info.dat', 'a') as positive, open('bg.txt', 'a') as negative:
            
            for row in cur:
                image_path = row[0]
                json_data = json.loads(row[1])
                
                if json_data['skip'] == 1:
                    continue
                
                rects = json_data['rects']
                
                #正例か負例か
                if len(rects) == 0:
                    negative.write('%s\n' % image_path)
                    continue

                s = self._format_rects(rects)
                positive.write('%s  %s\n' % (image_path, s))
        
        
        self._close(con, cur)
        
        
    def _format_rects(self, rects):
        
        rect_info = [str(len(rects))]
        
        for rect in rects:
            x = rect['startX']
            y = rect['startY']
            width = rect['endX']
            height = rect['endY']
            
            if width < 0:
                width = abs(width)
                x -= width
            if height < 0:
                height = abs(height)
                y -= height
            
            rect_info.append('%d %d %d %d' % (x, y, width, height))
            
            
        return '  '.join(rect_info)
        
    
    
    def _open(self):
        con = sqlite3.connect('train.db')
        cur = con.cursor()
        return (con, cur)
        
    def _close(self, con, cur):
        con.commit()
        cur.close()
        con.close()
    
    

if __name__ == '__main__':
    
    #画像の準備
    image_ptrn = re.compile('.*[.](jpg|jpeg|png)$', re.IGNORECASE)
    image_dir = os.path.join( 'static', 'img' )
    images = [ image.replace('\\', '/') for image in os.listdir( image_dir ) if re.match( image_ptrn, image ) ]
    if not images:
        exit( 'Error: Could not find images in "%s"' % image_dir)
        
    MyHandler.image_dir = image_dir
    MyHandler.images = sorted(images)
    
    server_address = ('0.0.0.0', 5000)
    httpd = HTTPServer(server_address, MyHandler)
    print('Serving HTTP on 0.0.0.0 port %d ...' % server_address[1])
    print('use <Ctrl-(C or break)> to stop')
    httpd.serve_forever()
    
