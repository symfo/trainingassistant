$(function() {
	
	var image;
	var canvas = document.getElementById('cnvs');
	var context = canvas.getContext('2d');
	var pos = 0;
	
    // 矩形オブジェクト
	var rects = [];
    var rect = createRect();
	
	function createRect() {
		return { startY:0, startX:0, endY:0, endX:0 };
	};
	
    function onMouseDown(e) {
        rect.startY = e.layerY;
        rect.startX = e.layerX;
		canvas.removeEventListener('mousemove', onMouseMoveGuide, false)
        canvas.addEventListener ('mousemove', onMouseMove, false);
    };

    function onMouseMove(e) {
        rect.endY = e.layerY - rect.startY;
        rect.endX = e.layerX - rect.startX;
		draw(rect);
    };
	
	function onMouseMoveGuide(e) {
		draw();
		context.strokeStyle = 'rgb(0, 255, 0)';
		context.lineWidth = 0.3;
		context.strokeRect(e.layerX, 0, 0, canvas.height);
		context.strokeRect(0, e.layerY, canvas.width, 0);
	}

    function onMouseUp(e) {
		// 高さ、幅が10px以上のデータのみ有効とみなす
		if ((Math.abs(rect.endX) > 10) && (Math.abs(rect.endY) > 10)) {
			rects.push(rect);
			draw();
		}
		rect = createRect();
        canvas.removeEventListener ('mousemove', onMouseMove, false);
		canvas.addEventListener ('mousemove', onMouseMoveGuide, false);
    };
	
	function draw(actRect) {
		context.drawImage(image, 0, 0);
		context.lineWidth = 3;
		context.strokeStyle = 'rgb(255, 0, 0)';
		rects.forEach(function(rect) {
			context.strokeRect(rect.startX, rect.startY, rect.endX, rect.endY);
		});
		if (actRect == null) {
			return;
		}
		context.strokeRect(actRect.startX, actRect.startY, actRect.endX, actRect.endY);
	};
	
	function onKeyUp(e) {
		switch(e.key) {
			case 'b':
				viewAjax('_back');
				break;
			case 'f':
				nextAjax(skip=0);
				break;
			case 's':
				nextAjax(skip=1);
				break;
			case 'r':
				rects = [];
				draw();
				break;
			case 'z':
				rects.pop();
				draw();
				break;
		};
	};
	
	function loadImage(imgsrc) {
		image = new Image();
		image.src = imgsrc;
		image.onload = function(){
			var width = image.naturalWidth;
			var height = image.naturalHeight;
			
			$('.main-wrapper').css({'width': width, 'minWidth': width});
			canvas.width = width;
			canvas.height = height;
			context.drawImage(image, 0, 0);
			draw();
		}
	};
	
	// --- イベント
	canvas.addEventListener('mousedown', onMouseDown, false);
	canvas.addEventListener('mouseup'  , onMouseUp  , false);
	canvas.addEventListener('mousemove'  , onMouseMoveGuide  , false);
	window.addEventListener('keyup'    , onKeyUp    , false);
	
	// --- ステータス描画
	function resetStatus(data){
		rects = data.rects;
		pos = data.pos;
		$('#counter').text(data.counter);
		$('#bar').css({'width': data.barwidth});
		loadImage(data.imgsrc);
	};
	
	function finishedStatus(total) {
		w = $('.head-wrapper').width();
		$('.main-wrapper').css({'width': w, 'minWidth': w});
		$('#canvas-wrapper')
			.empty()
			.append('<div class="messages">')
			.append('<div class="message">' + total + ' Images were</div><div class="message">Successfuly Processed!</div>');
		$('.btn').addClass('disabled');
		$('#bar').css({'width': '100%'});
	};
	
	
	// --- ボタン操作
	$('#next').on('click', function(){
		nextAjax(skip=0);
	});
	$('#skip').on('click', function(){
		nextAjax(skip=1);
	});
	$('#reset').on('click', function(){
		rects = [];
		draw();
	});
	$('#back').on('click', function(){
		viewAjax('_back');
	});
	
	
	// --- 通信
	function nextAjax(skip){
		var rects_json = JSON.stringify(rects);
		console.log('座標:'+rects_json);
		$.ajax({
			type: 'POST', 
			dataType: "json",
			data: {'rects': rects_json, 'skip': skip, 'pos':pos}, 
			url: '_next'
		}).done(function (data) {
			if (data.finished){
				finishedStatus(data.pos);
				return;
			}
			resetStatus(data);
		});
	};
	
	function viewAjax(url){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			data: {'pos':pos}, 
			url: url,
		}).done(function (data) {
			resetStatus(data);
		});
	};
	
	viewAjax('_init');
	
});