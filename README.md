## Set up

1. Clone to your computer: 


    % git clone https://symfo@bitbucket.org/symfo/trainingassistant.git


2. Add images into `static/img`


## Run server

    % cd TrainingAssistant
    % python views.py


This command starts the Flask server on port 5000, visit `http://localhost:5000`.

![ネコ可愛い](https://bytebucket.org/symfo/trainingassistant/raw/333e10d5575da88a0dd9e2f7f3ea2cd989338506/sample.png)

After all images will be processed, you will get `info.dat` and `bg.txt`; Respectively the list of `positive` samples and the list of `negative` samples.


fork from https://github.com/shkh/TrainingAssistant

